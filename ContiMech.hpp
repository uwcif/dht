/*! \brief Basic quantities often used for math in continuum mechanics
 *
 *  In this collection basic quantities are summariezed, which are generally
 *  needed for mathematical calculations within continuum mechanical theories
 *  
 *  Like the whole dht++ code, this part is also based on the Eigen library.
 */

#ifndef ContMech_H
#define ContMech_H



#include <Eigen>



//! Definition of the ContinuumsMechanicMath namespace **ContiMech**.
namespace ContiMech
{
  
  //! Doubles are used throughout the package
  typedef double T;
  //! General matrix for 4th-order tensor in normalized Voigt notation
  typedef Eigen::Matrix<T,6,6> Mat66;

  //! Calculation of Bulk modulus in double precision
  /*!
   *  \param young Young's modulus
   *  \param poisson Poisson's ratio
   *  \return Bulk modulus 
   *  \sa ShearModulus(const T, const T)
   */
  T BulkModulus( const T young, const T poisson ){
    return young/3.0/(1.0-2.0*poisson);
  }

  //! Calculation of Shear modulus in double precision
  /*!
   *  \param young Young's modulus
   *  \param poisson Poisson's ratio
   *  \return Shear modulus 
   */
  T ShearModulus( const T young, const T poisson ){
    return young/2.0/(1.0+poisson);
  }

  //! 4th-order identity tensor in normalized Voigt-Notation
  /*!
   * I4 = 1 box 1
   */
  Mat66 I4 = (Mat66(6,6) << 
              1, 0, 0, 0, 0, 0,
              0, 1, 0, 0, 0, 0,
              0, 0, 1, 0, 0, 0,
              0, 0, 0, 2, 0, 0,
              0, 0, 0, 0, 2, 0,
              0, 0, 0, 0, 0, 2).finished(); 

  //! Symmetric 4th-order identity tensor in normalized Voigt-Notation
  /*!
   * I4s = sym(1 box 1)
   */
  Mat66 I4s = (Mat66(6,6) <<  
               1, 0, 0, 0, 0, 0,
               0, 1, 0, 0, 0, 0,
               0, 0, 1, 0, 0, 0,
               0, 0, 0, 1, 0, 0,
               0, 0, 0, 0, 1, 0,
               0, 0, 0, 0, 0, 1).finished();

  //! First isotropic projector
  /*!
   *  P1 = I4s/3
   *
   *  Order: 11 22 33 23 13 12
   */
  Mat66 P1 = (Mat66(6,6) <<  
               1, 1, 1, 0, 0, 0,
               1, 1, 1, 0, 0, 0,
               1, 1, 1, 0, 0, 0,
               0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0).finished() / 3.0;

  //! Second isotropic projector
  /*!
   *  P2 = P1 - I4s
   *
   *  Order: 11 22 33 23 13 12
   */
  Mat66 P2 = I4s - P1;
}


#endif /* ContiMech_H */
