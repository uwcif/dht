#include "CStiffnessGeneral.cpp"
#include "CTensor.hpp"
#include "ContiMech.hpp"




/*! \brief Isotropic stiffness, defined via Young's modulus and Poisson's ratio
 * 
 * \author Viktor Müller
 * \date 2014-02-20
 * \version 0.0
 *
 */
class CStiffnessIsotropic: public CStiffnessGeneral
{
public:

  CStiffnessIsotropic();  //!< Default constructor

  /*! \brief  Constructor taking Young's modulus and Poisson's ratio as doubles
   *
   * \param young Young's modulus as double
   * \param poisson Poisson's ratio as double
   *
   */
  CStiffnessIsotropic( const double young, const double poisson );
  ~CStiffnessIsotropic(); //!< Default destructor
  
  /*! \brief Specialized definition to output engineer's constants
   *
   * \return Young's modulus and Poisson's ratio
   */
  double* GetEngineersConstants( );

  /*! \brief Specialized definition to set engineer's constants
   *
   * \param eng_const Young's modulus and Poisson's ratio
   */
  void SetEngineersConstants( double* eng_const);

private:
  
  /*! \brief Specialized definition. Only used for internal calculations!
  *
  */
  void SetOwnStiffness();
  
  double young;      // = 210.0; //!< Young's moduls with default definition
  double poisson;    // =   0.3; //!< Poisson's ratio with default definition

};


typedef double T;

CStiffnessIsotropic::CStiffnessIsotropic():CStiffnessGeneral()
{
  this->SetOwnStiffness();
}


/*!
 * Young's modulus and Poisson's are set. The stiffness is initialized.
 */
CStiffnessIsotropic::CStiffnessIsotropic( const T young, const T poisson ):CStiffnessGeneral()
{
  this->young   = young;
  this->poisson = poisson;
  this->SetOwnStiffness();
}

CStiffnessIsotropic::~CStiffnessIsotropic(){}
  
T* CStiffnessIsotropic::GetEngineersConstants( )
{
  return &((this->young,this->poisson))-1;   // Warum "-1"???
}

void CStiffnessIsotropic::SetEngineersConstants(T* eng_consts)
{
  this->young   = eng_consts[0];
  this->poisson = eng_consts[1];

  this->SetOwnStiffness();
}


void CStiffnessIsotropic::SetOwnStiffness()
{
  this->Stiff = 
    3.0 * ContiMech::BulkModulus(  this->young, this->poisson ) * ContiMech::P1 + 
    2.0 * ContiMech::ShearModulus( this->young, this->poisson ) * ContiMech::P2;
}