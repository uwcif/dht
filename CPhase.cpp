#include <iostream>
 // Use of library Eigen in shell:  -I /path/to/eigen/
//#include <Eigen> 
#include <string.h>
#include "CTensor.hpp"
#include "CStiffnessIsotropic.cpp"
#include "CThermalExpIsotropic.cpp"


using namespace std;
//using namespace Eigen;



/*! \brief Abstract class for general stiffness matrix definition
 * 
 *  \author Loredana Kehrer
 *  \date 2014-03-01 
 *  \version 0.0
 *
 *  Class for definition of different Phases.
 *  
 *  
 */

class CPhase {
public:

   CPhase();
  ~CPhase();
  
  //!< Base classes with virtual member functions allow only a pointer to their member functions
  CStiffnessGeneral  *CStiff_Phase;
  void                SetStiff_phase( CStiffnessGeneral & );
  CTensor<double,6,6> GetStiff_phase( );

  /*! \brief Base class with virtual member function.
   *
   * Access needs a poitner.
   */
  CThermalExpGeneral *Alpha_phase;
  void                SetAlpha_phase( CThermalExpGeneral & );
  CTensor<double,6,1> GetAlpha_phase( );
  
  /*! \brief Volume concetration of phase alpha.
   *
   */  
  double VolKonz_phase;

  /*! \brief Elastic strain of phase alpha.
   *
   */
  CTensor<double,6,1> Strain_elastic;
  
  /*! \brief Localization of elastic strain of phase alpha.
   *
   */
  CTensor<double,6,6> Loc_strain;
  
  /*! \brief Eigen strain of phase alpha.
   *
   * This can be thermal strain or plastic strain.
   */
  CTensor<double,6,1> Strain_eigen;

   /*! \brief Localization of eigen strain of phase alpha.
   *
   */
  CTensor<double,6,1> Loc_eigen;

   /*! \brief Localization of stress of phase alpha.
   *
   */
  CTensor<double,6,1> Stress_loc;

   /*! \brief Localization of eigen stress of phase alpha.
   *
   */
  CTensor<double,6,1> Stress_eigen;

  /*! \brief Euler-angles in z-x-z-convention
   *
   */
  CTensor<double,3,1> Orientation;
  
  /*! Rotation matrix with help of Euler-angles
   *
   */
  CTensor<double,3,3> RotationMatrix;

  /*! \brief Set rotation matrix
   *
   */  
  void SetRotationMatrix();

  /*! \brief Rotate Stiffness
   *
   */
  void RotateStiffness();

  //  CTensor<double,?,?> A_Geom;

  

};

CPhase::CPhase(){}
CPhase::~CPhase(){}



void                CPhase::SetStiff_phase( CStiffnessGeneral & SomeStiffness)
{
/*! \brief 
  Assignment of adress:
  Variable \param CStiff_Phase is a pointer to an object of type CStiffnessGeneral. It has the adress of this object.
  This routine uses "pass-by-reference", because it is more friendly for a user. The user (of a main-routine) 
  passes only an object of type CStiffGeneral.
  In this function the &-operator passes the adress of an object and overwrites the pointer. Now the pointer CStiff_Phase points to the 
  passed object.
*/

    this->CStiff_Phase = &SomeStiffness;
    /// For Testting:
    /// After set equal, the value of pointer CStiff_Phase has to be the same as the adress of variable SomeStiffness:
    // cout << endl << "Nach dem Gleichsetzen folgt:" << endl;
    // cout << "Speicherplatz von SomeStiffness: "             << &SomeStiffness << endl;
    // cout << "Auf diesen Speicherplatz zeigt CStiff_Phase: " << CStiff_Phase   << endl;

/** \brief remark to syntax:
  The following commands should lead to the same results:
     cout << endl << SomeStiffness.GetStiffness()   << endl;
     cout << endl << (*CStiff_Phase).GetStiffness() << endl;
     cout << endl << CStiff_Phase->GetStiffness()   << endl;
*/

}



CTensor<double,6,6> CPhase::GetStiff_phase( )
{
  return ( this->CStiff_Phase->GetStiffness() );
}



void                CPhase::SetAlpha_phase( CThermalExpGeneral & SomeAlpha)
{
    this->Alpha_phase = &SomeAlpha;
}



CTensor<double,6,1> CPhase::GetAlpha_phase( )
{
  return ( this->Alpha_phase->GetThermalExp() );
}


/*! \brief  Definition of rotation matrizes Q1, Q2, Q3 (cf. Boehlke Plastizitaetstheorie-Skript)
 *
 */
void                CPhase::SetRotationMatrix()
{
        const double theta1 = Orientation[0];
        const double theta2 = Orientation[1];
        const double theta3 = Orientation[2];
        
        CTensor<double,3,3> Q1;
        CTensor<double,3,3> Q2;
        CTensor<double,3,3> Q3;
        
        
	Q1 << cos(theta1),  -sin(theta1),   0.,
	      sin(theta1),  cos(theta1),    0.,
	      0.,           0.,             1.;


	
        Q2 << 1.,           0.,             0.,
	      0.,           cos(theta2),    -sin(theta2),
	      0.,           sin(theta2),    cos(theta2);


        Q3 << cos(theta3),  -sin(theta3),   0.,
              sin(theta3),  cos(theta3),    0.,
	      0.,           0.,             1.;


        RotationMatrix = Q1*Q2*Q3;
}


void                CPhase::RotateStiffness()
{
//   cout << endl << "START: RotateStiffness: ";
//   cout << "In dieser Routione soll der Steifigkeitstensor mithilfe der RotationMatrix gedreht werden." << endl;
//   cout << "Die RotationMatrix lautet: " << endl << this->RotationMatrix << endl;
//   cout << "Der Steifigkeitstensor lautet: " << endl << this->CStiff_Phase->GetStiffness() << endl;
//   cout <<         "ENDE: RotateStiffness: " << endl;
}


