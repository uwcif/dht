#include "CThermalExpIsotropic.cpp"
#include "CTensor.hpp"
#include "ContiMech.hpp"


int main(){
  /// Define 1d-Array for a later use
T *ThermalConstants;


/// Initialization of Alpha-Tensor
CThermalExpIsotropic A1(3.0);


/// Testing GetThermalExp ( Function of parental class)
cout << endl << "START --> Teste GetThermalExp:" << endl;
cout         << "Der Alpha-Tensor lautet in Voigt-Notation: " << endl << A1.GetThermalExp() << endl;
cout         << "ENDE --> Teste GetThermalExp" << endl;


/// Testing GetThermalExp ( Function of parental class)
///\warning Die Memberfunktion "double GetThermalExp(int i, int j)" der
///         Elternklasse funktioniert nicht richtig! Wahrscheinlich wgn der
///         verwendeten Voigt-Notation ???
///         Fehler tritt nicht beim Kompilieren, sondern waehrend der Laufzeit auf!!!
// cout << endl << "START --> Teste GetThermalExp:" << endl;
// cout         << "Der Alpha-Tensor lautet in Voigt-Notation: " << endl << A1.GetThermalExp(1,2) << endl;
// cout         << "ENDE --> Teste GetThermalExp" << endl;


/// Testing GetThermalConstants ( pure virtual in parental class )
ThermalConstants = A1.GetThermalConstants();
cout << endl << "START --> Teste GetThermalConstants:" << endl;
cout         << "Die thermischen Konstanten lauten: "  << endl << ThermalConstants[0] << endl;
cout         << "ENDE --> Teste GetThermalConstants"   << endl;


/// Testing SetThermalExp (  Function of parental class)
CTensor<double, 6, 1>  A_Direkt;
                       A_Direkt.resize(6,1);
                       A_Direkt<< 1.0, 1.0, 1.0, 0.0, 0.0, 0.0;
                       A1.SetThermalExp( A_Direkt );
cout << endl << "START --> Teste SetThermalExp:"              << endl;
cout << endl << "Der Alpha-Tensor lautet in Voigt-Notation: " << endl << A1.GetThermalExp() << endl;
ThermalConstants = A1.GetThermalConstants();
cout         << "PROBLEM: Die thermischen Konstanten lauten: "         << endl << ThermalConstants[0] << endl;
cout         << "ENDE --> Teste SetThermalExp"                << endl;


/// Testing SetThermalConstants ( pure virtual in parental class )
ThermalConstants[0] = 2;
A1.SetThermalConstants( ThermalConstants);
cout << endl << "START --> Teste SetThermalConstants:"        << endl;
cout << endl << "Die thermischen Konstanten lauten: "         << endl << ThermalConstants[0] << endl;
cout << endl << "Der Alpha-Tensor lautet in Voigt-Notation: " << endl << A1.GetThermalExp() << endl;
cout         << "ENDE --> Teste SetThermalConstants"          << endl;

}
