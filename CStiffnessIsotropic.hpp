
#include <CStiffnessGeneral.hpp>
#include <CTensor.hpp>

#ifndef CSTIFFNESSISOTROPIC_H
#define CSTIFFNESSISOTROPIC_H


/*! \brief Isotropic stiffness, defined via Young's modulus and Poisson's ratio
 * 
 * \author Viktor Müller
 * \date 2014-02-20
 * \version 0.0
 *
 */
class CStiffnessIsotropic: public CStiffnessGeneral
{
public:

  CStiffnessIsotropic();  //!< Default constructor

  /*! \brief  Constructor taking Young's modulus and Poisson's ratio as doubles
   *
   * \param young Young's modulus as double
   * \param poisson Poisson's ratio as double
   *
   */
  CStiffnessIsotropic( const double young, const double poisson );
  ~CStiffnessIsotropic(); //!< Default destructor
  
  /*! \brief Specialized definition to output engineer's constants
   *
   * \return Young's modulus and Poisson's ratio
   */
  double* GetEngineersConstants( );

  /*! \brief Specialized definition to set engineer's constants
   *
   * \param eng_const Young's modulus and Poisson's ratio
   */
  void SetEngineersConstants( double* eng_const);

private:
  
  void SetOwnStiffness();
  
  double young   = 210.0; //!< Young's moduls with default definition
  double poisson =   0.3; //!< Poisson's ratio with default definition

};

#endif /* CSTIFFNESSISOTROPIC_H */
