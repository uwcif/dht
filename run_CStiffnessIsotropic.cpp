#include "CStiffnessIsotropic.cpp"
#include "CTensor.hpp"
#include "ContiMech.hpp"



int main ()
{
//   /// Testing if the  main-routine is called the right way:
//   cout << endl << "Aufruf der main-Routine funktioniert" << endl;
  
  /// Testing of initialization of CIso
  CStiffnessIsotropic C1(210.0, 0.3);
  
  /// Testing of GetStiffness():
  cout << endl << C1.GetStiffness() << endl;
  
  /// Testing of GetEngineersConstants():
  T *a = (C1.GetEngineersConstants() );
  cout << endl << a[0] << "  " << a[1] << endl;
}
