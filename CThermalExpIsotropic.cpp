#include "CThermalExpGeneral.cpp"
#include "CTensor.hpp"
#include "ContiMech.hpp"




/*! \brief Isotropic stiffness, defined via Young's modulus and Poisson's ratio
 * 
 * \author Loredana Kehrer
 * \date 2014-03-12
 * \version 0.0
 *
 */
class CThermalExpIsotropic: public CThermalExpGeneral
{
public:

  CThermalExpIsotropic();  //!< Default constructor

  /*! \brief  Constructor taking thermal constant a1 for thermal isotropy
   *
   * \param a1 double, thermal constant for thermal isotropy
   *
   */
  CThermalExpIsotropic( const double a1 );
  ~CThermalExpIsotropic(); //!< Default destructor
  
  /*! \brief Specialized definition. Output thermal constants.
   * 
   * \return a1: thermal constant for thermal isotropy
   */
  double* GetThermalConstants( );

  /*! \brief Specialized definition. Set thermal constants.
   *
   * \param therm_const double 1d-Array, contains only a1 -> therm_const[0] == a1
   */
  void SetThermalConstants( double* therm_const);

  /*! \brief Specialized definition. Set thermal expansion directly.
   *  Can be used for overloading operator "=" !
   */
  void SetThermalExp(CTensor<double, 6, 1> &newAlpha );

private:
  /*! \brief Specialized definition. Only used for internal calculations!
   *
   */
  void SetOwnAlpha();
  
  double a1;      // = 11.8; //!< thermal constant with default definition

};


typedef double T;

CThermalExpIsotropic::CThermalExpIsotropic():CThermalExpGeneral()
{
  this->SetOwnAlpha();
}


/*! \brief
 * Thermal constant a1 is set. The stiffness is initialized.
 *
 * Alternative: possible to write: " CThermalExpIsotropic::CThermalExpIsotropic( const T a1 )"
 * Which means that it is not referencing to default-constructor of parent-class.
 * cf. http://www.cplusplus.com/doc/tutorial/inheritance/
 */
CThermalExpIsotropic::CThermalExpIsotropic( const T a1 ):CThermalExpGeneral()
{
  this->a1   = a1;
  this->SetOwnAlpha();
}

CThermalExpIsotropic::~CThermalExpIsotropic(){}
  
T* CThermalExpIsotropic::GetThermalConstants( )
{
//   return &((this->a1))-1;   // Warum "-1"???
  return &((this->a1));
}

void CThermalExpIsotropic::SetThermalConstants(T* therm_const)
{
  this->a1   = therm_const[0];

  this->SetOwnAlpha();
}

void CThermalExpIsotropic::SetThermalExp(CTensor<double,6,1> &newAlpha)
{

  /*! This member function can ONLY be calculated if:
   *  \li newAlpha(0,0) == newAlpha(1,0)
   *  \li newAlpha(0,0) == newAlpha(2,0)
   *  \li newAlpha(1,0) == newAlpha(2,0)
   *  \li newAlpha(i,0) == 0.0 \forall i > 2
   *  This constraint is because the class is a thermal isotropic one 
   */

  if ( newAlpha(0,0) == newAlpha(1,0) && newAlpha(0,0) == newAlpha(2,0) && newAlpha(3,0) == 0.0 && newAlpha(4,0) == 0.0 && newAlpha(5,0) == 0.0 )
  {
    this->a1    = newAlpha(0,0);

    this->Alpha = newAlpha;
  }
  else
  {
    cout << endl << endl;
    cout << "ACHTUNG: FEHLER IN DER FUNKTION SetThermalExp(CTensor<double,6,1> &newAlpha) !!!" << endl;
    cout << "SetThermalExp IST MEMBERFUNKTION DER KLASSE CThermalExpIsotropic !!!" << endl;
    cout << "GRUND:   ES WURDE KEIN ISOTROPER WAERMEAUSDEHNUNGSTENSOR UEBERGEBEN !!!" << endl;
    cout << endl << endl;
    throw;
  }
}

void CThermalExpIsotropic::SetOwnAlpha()
{
  this->Alpha << a1, a1, a1, 0, 0, 0;
}

