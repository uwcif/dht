#include "CPhase.cpp"


int main()
{
   /// Initialization of a  Stiffness tensor
   CStiffnessIsotropic S1(210.0,0.3);

   
   /// Initialization of a phase
   CPhase P1;

   
   /// Testing of  SetStiff_phase: Set S1 to the phase of the stiffness tensor
   P1.SetStiff_phase( S1 );
   
   
   /// Testing GetStiffness ( ATTENTION WITH SYNTAX!!!)
   /// @warning If the following line is uncommented, you get other values for GetAlpha_phase!!
   
   cout << endl << "# Teste GetStiffness" << endl;
   cout << P1.CStiff_Phase->GetStiffness()<< endl <<endl;
   
   
   /// Testing GetEngineersConstants ( ATTENTION WITH SYNTAX!!!)
   T *a = P1.CStiff_Phase->GetEngineersConstants();
   cout << endl << "# Teste GetEngineersConstants" << endl;
   cout << a[0] << "  " << a[1] << endl << endl;

   
   /// Testing GetStiff_phase
   CTensor<double,6,6> A;
                       A = P1.GetStiff_phase( );
   cout << endl << "# Teste GetStiff_phase" << endl;
   cout << A << endl << endl;
   
   
   /// Initialization of an  Alpha-tensor
   CThermalExpIsotropic A1(3.0);
   cout << endl << "# Initialisieren eines Alpha-Tensors: " << endl << A1.GetThermalExp() << endl;


   /// Testing SetAlpha_phase: Set A1 to the phase of Alpha-tensor
   P1.SetAlpha_phase( A1);
   
   
   /// Testing GetAlpha_phase: return of Alpha-Tensors
   CTensor<double,6,1> B;
                       B = P1.GetAlpha_phase();
   cout << endl << "# Teste GetAlpha_phase" << endl;
   cout << B << endl << endl;
   
   
   /// Testing Euler-angles (set)
   P1.Orientation  << 1, 2, 3;


   /// Teste Euler-angles (return)
   cout << endl << "# Teste Ausgabe der Eulerwinkel" << endl;
   cout << P1.Orientation << endl <<endl;
   
   
   /// Testing SetRotationMatrix: Set of RotationMatrix for Orientation (s.o)
   P1.SetRotationMatrix();


   /// Testing return of RotationMatrix
   cout << endl << "# Teste Ausgabe der RotationMatrix" << endl;
   cout << P1.RotationMatrix << endl << endl;

   
   /// Testing RotateStiffness
   P1.RotateStiffness();

  
   
}
