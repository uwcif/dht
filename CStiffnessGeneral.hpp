#include <CTensor.hpp>

#ifndef CSTIFFNESSGENERAL_H
#define CSTIFFNESSGENERAL_H

/*! \brief Abstract class for general stiffness matrix definition
 * 
 *  \author Viktor Müller
 *  \date 2014-02-20 
 *  \version 0.0
 *
 *  Base class for definition of material stiffness. The default convention
 *  is the normalized Voigt notation. 
 *  This class is an abstract class.
 */
class CStiffnessGeneral {
public:

  CStiffnessGeneral();  //!< Default constructor
  ~CStiffnessGeneral(); //!< Default destructor

  /*! \brief Get the stiffness in non-normalized Voigt notation
   *
   * The stiffness in Voigt notation is always calculated from Stiff.
   */
  CTensor<double,6,6> GetInVoigtNotation( );

  /*! \brief Get the stiffness in normalized Voigt notation (default)
   *
   *  
   */
  CTensor<double,6,6> GetStiffness( );

  /*! \brief Get the component of the stiffness at the position (\a i,\a j)
   *
   */
  double GetStiffness( const int i, const int j );


  /*! \brief Get the compliance in normalized Voigt notation
   *
   *  The compliance is not stored. 
   *  Each time this function is called the compliance is calculated.
   */
  CTensor<double,6,6> GetCompliance( );
  
  /*! \brief Set the stiffness directly
   *
   *  \param stiff is of CTensor type
   */
  void SetStiffness( CTensor<double,6,6>& stiff );

  /*! \brief Set only one component of the  stiffness directly
   *
   */
  void SetStiffness( const int, const int, double );

  /*! \brief Virtual function. Gives engineer's constants
   *
   *  To define in derived classes
   */
  virtual double *GetEngineersConstants( ) = 0;

  /*! \brief Virtual function. Sets engineer's constants
   *
   *  To define in derived classes
   */
  virtual void SetEngineersConstants( double* ) = 0;
  
protected:

  /*! \brief Virtual function. Only used for internal calculations!
   *
   *  To define in derived classes
   */
  virtual void SetOwnStiffness() = 0;

  /*! \brief Stiffness in normalized Voigt notation
   *
   */
  CTensor<double,6,6> Stiff;

private:

  /*! \brief Factor, used for notation conversion
   *
   *  Used to convert stiffnesses in normalized Voigt notation to the
   *  not-normalized notation.
   */
  const CTensor<double,6,6> ToVoigtNotationFactor = ( CTensor<double,6,6>() << 
                                                 1, 1, 1, 1/sqrt(2), 1/sqrt(2), 1/sqrt(2),
                                                 1, 1, 1, 1/sqrt(2), 1/sqrt(2), 1/sqrt(2),
                                                 1, 1, 1, 1/sqrt(2), 1/sqrt(2), 1/sqrt(2),
                                                 1/sqrt(2), 1/sqrt(2), 1/sqrt(2), 0.5, 0.5, 0.5,
                                                 1/sqrt(2), 1/sqrt(2), 1/sqrt(2), 0.5, 0.5, 0.5,
                                                 1/sqrt(2), 1/sqrt(2), 1/sqrt(2), 0.5, 0.5, 0.5 ).finished();
};

#endif /* CSTIFFNESSGENERAL_H */
