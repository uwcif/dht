#include <iostream>
//#include <Eigen> 
#include <string.h>
#include "CTensor.hpp"


using namespace std;
using namespace Eigen;


/*! \brief Abstract class for general stiffness matrix definition
 * 
 *  \author Loredana Kehrer
 *  \date 2014-02-28 
 *  \version 0.0
 *
 *  Base class for definition of thermal expansion.
 *  This class is an abstract class.
 */

class CThermalExpGeneral {
  
public:
   CThermalExpGeneral();    //!< Default constructor
  ~CThermalExpGeneral();    //!< Default destructor

   /*! \brief Get the thermal expansion
    *
    */
  CTensor<double,6,1> GetThermalExp( );

  /*! \brief Get the component of the thermal expansion tensor at (\a i, \a j)
   *
   */
  double GetThermalExp(int i, int j);

  /*! \brief Virtual function. Set thermal expansion directly
   *
   */
  virtual void SetThermalExp(CTensor<double, 6, 1> &newAlpha ) = 0;

  /*! \todo SetThermalExp by one component value --> necessary?
   *
   */

  /*! \brief Virtual function. Gives thermal constants
   *
  *  To define in derived classes
   */
  virtual double *GetThermalConstants( ) = 0;

  /*! \brief Virtual function. Set thermal constants
   *
  *  To define in derived classes
   */
  virtual void SetThermalConstants( double* ) = 0;

protected:

  /*! \brief Virtual function. Only used for internal calculations!
   *
   *  To define in derived classes
   */
  virtual void SetOwnAlpha() = 0;
  
  CTensor<double, 6, 1> Alpha;
  

};

CThermalExpGeneral::CThermalExpGeneral(){ }   //!< Declaration of default constructor 

CThermalExpGeneral::~CThermalExpGeneral(){}  //!< Default destructor

CTensor<double,6,1> CThermalExpGeneral::GetThermalExp( )
{
  
  return this->Alpha;
}

double CThermalExpGeneral::GetThermalExp( const int i, const int j)
{
 
  return this->Alpha(i,j);
}


