#include<Eigen>

#ifndef CTENSOR_H
#define CTENSOR_H

/*! \brief Template Class defining Tensor in same manner as Eigen::Matrix
 *
 * This template class inherits all properties of Eigen::Matrix and should be 
 * used as such.
 * Today, 19.02.2014, this class does not have additional functionality compared
 * to Eigen::Matrix. Nevertheless, in the future, this class can be used to
 * define tensor operations: dyadic product, box product, etc.
 * \param T: type of entries (float, double, ...)
 * \param rows, cols: number of rows and columns
 * 
 */
template <typename T, const int rows, const int cols> class CTensor : public Eigen::Matrix<T,rows,cols>
{
public:
  
  //! General Constructor
  CTensor(void):Eigen::Matrix<T,rows,cols>() {}

  typedef Eigen::Matrix<T,rows,cols> Base;

  //! This constructor allows you to construct CTensor from Eigen expressions
  template<typename OtherDerived>
  CTensor(const Eigen::MatrixBase<OtherDerived>& other)
    : Eigen::Matrix<T,rows,cols>(other)
  { }

  //! This method allows you to assign Eigen expressions to CTensor
  template<typename OtherDerived>
  CTensor & operator= (const Eigen::MatrixBase <OtherDerived>& other)
  {
    this->Base::operator=(other);
    return *this;
  }  
};

#endif /* CTENSOR_H */
